import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class DonorDetailsScreen extends StatelessWidget {
  static final routeName = "/donor_details";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          topView(context),
          SingleChildScrollView(
            child: bottomView(context),
          )
        ],
      ),
    );
  }

  topView(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return Container(
      width: double.infinity,
      decoration: new BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xfffbfbfb), Color(0xfff2f2f2)],
          stops: [0, 1],
          begin: Alignment(-0.00, -1.00),
          end: Alignment(0.00, 1.00),
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            height: statusBarHeight,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, top: 16),
              child: Row(
                children: [
                  SvgPicture.asset('assets/images/ic_back_arrow.svg'),
                  SizedBox(
                    width: 14,
                  ),
                  ViewUtil.text(
                      "Donor Details", 16, FontWeight.w700, Colors.black),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              SvgPicture.asset('assets/images/ic_group_background.svg'),
              Container(
                child:
                    ViewUtil.text("A+", 22, FontWeight.w600, Color(0xffd1001c)),
                padding: EdgeInsets.only(
                  bottom: 16,
                  left: 8,
                ),
              )
            ],
          ),
          ViewUtil.text(
              "Mohammad Masudur Rahman", 15, FontWeight.w500, Colors.black),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  bottomView(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30),
      child: Column(
        children: [
          SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  ViewUtil.text(
                      "Phone number", 11, FontWeight.w400, Color(0xff94989e)),
                  SizedBox(
                    height: 6,
                  ),
                  ViewUtil.text(
                      "+01911967811", 14, FontWeight.w500, Colors.black),
                ],
              ),
              GestureDetector(
                child:   ViewUtil.circularBoxContainer(
                  context,
                  80,
                  36,
                  20,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.call,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      ViewUtil.text("Call", 12, FontWeight.w600, Colors.black),
                    ],
                  ),
                ),
                onTap: () => {_makePhoneCall('tel:+8801521401599')},
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Divider(
            height: 1,
            color: Color(0xffe6e6e6),
          ),
          _bottomViewHelper("Email Address", "masudur.rahman@sslwireless.com"),
          _bottomViewHelper("Company", "SSL wireless"),
          _bottomViewHelper("Employee ID", "1111"),
          _bottomViewHelper("Department", "Finance & Accounts"),
          _bottomViewHelper("Designation", "Assistant Manager"),
        ],
      ),
    );
  }

  _bottomViewHelper(String header, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 16,
        ),
        ViewUtil.text(header, 11, FontWeight.w400, Color(0xff94989e)),
        SizedBox(
          height: 6,
        ),
        ViewUtil.text(value, 14, FontWeight.w500, Colors.black),
        SizedBox(
          height: 16,
        ),
        Divider(
          height: 1,
          color: Color(0xffe6e6e6),
        ),
      ],
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
