import 'package:bloodfinder/features/filter/model/company.dart';
import 'package:bloodfinder/features/filter/model/filterData.dart';
import 'package:bloodfinder/features/filter/model/gender.dart';
import 'package:bloodfinder/global_model/model.dart';
import 'package:get/get.dart';

class FilterController extends GetxController {
  final groups = bloodGroupsWithOutAll;
  var selectedBloodGroupIndex = RxInt(-1);
  var selectedGenderIndex = RxInt(-1);

  var companyData = companies;

  var previousSelectedBloodGroup = "";
  var selectedCompanies = <String>[];
  var previousSelectedGender = "";

  final RxList<String> selectedFilterData = <String>[].obs;

  var filterData = FilterData();

  getSelectedFilterData() {
    selectedFilterData.clear();

    selectedFilterData.add(filterData.bloodGroup);
    filterData.companies.forEach((element) {
      var companyWithDept = element.name;

      companyWithDept += "(";
      element.depts.forEach((d) {
        if (d.isSelected) {
          companyWithDept += "${d.title},";
        }
      });
      companyWithDept += ")";

      selectedFilterData.add(companyWithDept);
    });
    selectedFilterData.add(filterData.gender);

    selectedFilterData.removeWhere((element) => element == "");

    return selectedFilterData;
  }

  clear() {
    selectedBloodGroupIndex = RxInt(-1);
    selectedGenderIndex = RxInt(-1);
    selectedFilterData.clear();
    selectedCompanies.clear();
    previousSelectedBloodGroup = "";
    previousSelectedGender = "";

    filterData = FilterData();

    companyData.forEach((element) {
      element.isSelected = false;
      element.depts.forEach((d) {
        d.isSelected = false;
      });
    });
  }

  final genders = <Gender>[
    Gender("Men"),
    Gender("Women"),
  ];
}
