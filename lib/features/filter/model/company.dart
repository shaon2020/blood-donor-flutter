import 'package:bloodfinder/features/filter/model/department.dart';
import 'package:flutter/material.dart';

class Company {
  final String name;
  final List<Department> depts;
  var isSelected = false;

   Company({
    @required this.name,
    @required this.depts
  });
}

final companies = [
  Company(name: "Concrode Garments Ltd.", depts:  [Department(title: "Engineering")]),
  Company(name: "Bidyut Ltd.", depts:  [Department(title: "Hr"),Department(title: "Accounts")]),
  Company(name: "SSL Wireless", depts:  [Department(title: "Finance"),Department(title: "Networking")])
];
