import 'package:bloodfinder/features/filter/model/company.dart';
import 'package:bloodfinder/features/filter/model/department.dart';
import 'package:bloodfinder/features/filter/model/gender.dart';
import 'package:bloodfinder/global_model/model.dart';

class FilterData {
   String bloodGroup = "";
   List<Company> companies = [];
   List<Department> depts = [];
   String gender = "";
}