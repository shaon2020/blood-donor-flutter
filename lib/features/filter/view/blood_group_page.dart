import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BloodGroupPage extends StatelessWidget {
  final FilterController filterController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 34, right: 34, top: 40, bottom: 40),
      child: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 26,
        mainAxisSpacing: 40,
        children: List.generate(
          filterController.groups.length,
          (index) {
            return Obx(() => GestureDetector(
                  onTap: () {
                    if (filterController.selectedBloodGroupIndex.value ==
                        index) {
                      filterController.selectedBloodGroupIndex.value = -1;

                      filterController.filterData.bloodGroup = "";
                    } else {
                      filterController.selectedBloodGroupIndex.value = index;

                      filterController.filterData.bloodGroup =
                          filterController.groups[index];
                    }

                    filterController.getSelectedFilterData();
                  },
                  child: Container(
                    child:
                        filterController.selectedBloodGroupIndex.value == index
                            ? ViewUtil.circularBoxContainerWithColor(
                                context,
                                80,
                                80,
                                100,
                                Center(
                                  child: ViewUtil.text(
                                    filterController.groups[index],
                                    18,
                                    FontWeight.w600,
                                    Colors.white,
                                  ),
                                ),
                                Theme.of(context).accentColor,
                              )
                            : Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Color(0xffe6e6e6),
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                child: Center(
                                  child: ViewUtil.text(
                                    filterController.groups[index],
                                    18,
                                    FontWeight.w600,
                                    Color(0xff686d73),
                                  ),
                                ),
                              ),
                  ),
                ));
          },
        ),
      ),
    );
  }
}
