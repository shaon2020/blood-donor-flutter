import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/features/filter/model/company.dart';
import 'package:bloodfinder/features/filter/model/department.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CompanyPage extends StatelessWidget {
  final FilterController filterController = Get.find();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, index) {
        return MyExpandableWidget(filterController.companyData[index]);
      },
      itemCount: filterController.companyData.length,
    );
  }
}

class MyExpandableWidget extends StatefulWidget {
  final Company company;

  MyExpandableWidget(this.company);

  @override
  _MyExpandableWidgetState createState() => _MyExpandableWidgetState();
}

class _MyExpandableWidgetState extends State<MyExpandableWidget> {
  final FilterController filterController = Get.find();

  @override
  Widget build(BuildContext context) {
    final expandWidgets = <Widget>[
      SizedBox(
        width: double.infinity,
        child: Container(
          padding: EdgeInsets.only(left: 70, top: 14),
          color: const Color(0xfff5f5f5),
          child: ViewUtil.text(
              "Select Department", 11, FontWeight.w400, Color(0xffadb1b2)),
        ),
      )
    ];
    expandWidgets
        .addAll(widget.company.depts.map((e) => getDeptView(e)).toList());

    return ExpansionTile(
      title:
          ViewUtil.text(widget.company.name, 14, FontWeight.w700, Colors.black),
      leading: GestureDetector(
        child: widget.company.isSelected
            ? SvgPicture.asset("assets/images/ic_oval_correct.svg")
            : SvgPicture.asset("assets/images/ic_circle_border.svg"),
        onTap: () {
          // Working with company selection

          setState(() {
            widget.company.isSelected = !widget.company.isSelected;

            widget.company.depts.forEach((element) {
              if (widget.company.isSelected)
                element.isSelected = true;
              else
                element.isSelected = false;
            });

            if (widget.company.isSelected) {
              filterController.filterData.companies.add(widget.company);
            } else {
              filterController.filterData.companies.remove(widget.company);
            }

            filterController.getSelectedFilterData();
          });
        },
      ),
      children: expandWidgets,
    );
  }

  Widget getDeptView(Department e) {
    FilterController filterController = Get.find();

    return Container(
      padding: EdgeInsets.only(left: 60),
      color: const Color(0xfff5f5f5),
      child: ListTile(
        leading: GestureDetector(
          child: e.isSelected
              ? SvgPicture.asset("assets/images/ic_oval_correct.svg")
              : SvgPicture.asset("assets/images/ic_circle_border.svg"),
          onTap: () {
            setState(() {
              e.isSelected = !e.isSelected;

              var depts2 = widget.company.depts;
              var isAllDeptSelected = true;
              for (var i = 0; i < depts2.length; i++) {
                if (!depts2[i].isSelected) {
                  isAllDeptSelected = false;
                  break;
                }
              }
              widget.company.isSelected = isAllDeptSelected;

              if (isAllDeptSelected) {
                if (!filterController.filterData.companies
                    .contains(widget.company))
                  filterController.filterData.companies.add(widget.company);
              } else {
                var isStillOneSelected = false;
                for (var i = 0; i < depts2.length; i++) {
                  if (depts2[i].isSelected) {
                    isStillOneSelected = true;
                    break;
                  }
                }

                if (isStillOneSelected) {
                  if (!filterController.filterData.companies
                      .contains(widget.company))
                    filterController.filterData.companies.add(widget.company);
                } else
                  filterController.filterData.companies.remove(widget.company);
              }

              filterController.getSelectedFilterData();
            });
          },
        ),
        title: Container(
          child: ViewUtil.text(
            e.title,
            12,
            FontWeight.w400,
            Color(0xff808283),
          ),
        ),
      ),
    );
  }
}
