import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/features/filter/view/blood_group_page.dart';
import 'package:bloodfinder/features/filter/view/company_page.dart';
import 'package:bloodfinder/features/filter/view/gender_page.dart';
import 'package:bloodfinder/features/home/view/home_screen.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class FilterMainScreen extends StatelessWidget {
  static final routeName = "/filter";

  final filterController = Get.put(FilterController());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Row(
              children: [
                SvgPicture.asset('assets/images/ic_back_arrow.svg'),
                SizedBox(
                  width: 14,
                ),
                ViewUtil.text("Filter by", 16, FontWeight.w700, Colors.black),
              ],
            ),
          ),
          bottom: TabBar(
            tabs: [
              Tab(
                text: "Blood Group",
              ),
              Tab(
                text: "Company",
              ),
              Tab(
                text: "Gender",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            BloodGroupPage(),
            CompanyPage(),
            GenderPage(),
          ],
        ),
        bottomNavigationBar: bottomView(context),
      ),
    );
  }

  bottomView(BuildContext context) {
    return Obx(() => SizedBox(
          height: 100,
          width: double.infinity,
          child: Column(
            children: [
              Container(
                height: 44,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(color: Color(0x19000000), blurRadius: 12)
                  ],
                ),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: filterController.selectedFilterData.value.length,
                  itemBuilder: (context, index) {
                    return Center(
                      child: Container(
                        child: ViewUtil.text(
                            filterController.selectedFilterData.value[index],
                            11,
                            FontWeight.w500,
                            Color(0xff686d73)),
                        margin: index == 0
                            ? EdgeInsets.only(left: 20, top: 8, bottom: 8)
                            : EdgeInsets.only(left: 6, top: 8, bottom: 8),
                        padding: EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 8),
                        decoration: BoxDecoration(
                          color: Color(0xffededed),
                          borderRadius: BorderRadius.circular(13),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 56,
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).accentColor),
                  onPressed: () {
                    Get.offAllNamed(HomeScreen.routeName);
                  },
                  child:
                      ViewUtil.text("Apply", 14, FontWeight.w700, Colors.white),
                ),
              ),
            ],
          ),
        ));
  }
}
