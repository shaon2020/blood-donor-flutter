import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/features/filter/model/gender.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class GenderPage extends StatefulWidget {
  @override
  _GenderPageState createState() => _GenderPageState();
}

class _GenderPageState extends State<GenderPage> {
  final FilterController filterController = Get.find();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: filterController.genders.length,
        itemBuilder: (ctx, index) {
          return ListTile(
            onTap: () {
              setState(() {
                filterController.genders[index].isSelected =
                    !filterController.genders[index].isSelected;

                if (filterController.selectedGenderIndex.value  ==
                    index) {
                  filterController.selectedGenderIndex.value  = -1;
                  filterController.filterData.gender = "";
                } else {
                  filterController.selectedGenderIndex.value = index;

                  filterController.filterData.gender =
                      filterController.filterData.gender =
                      filterController.genders[index].title;
                }

                filterController.previousSelectedGender =
                    filterController.genders[index].title;


                filterController.getSelectedFilterData();
              });
            },
            leading: filterController.selectedGenderIndex.value == index
                ? SvgPicture.asset("assets/images/ic_oval_correct.svg")
                : SvgPicture.asset("assets/images/ic_circle_border.svg"),
            title: ViewUtil.text(
              filterController.genders[index].title,
              14,
              FontWeight.w700,
              Color(0xff000000),
            ),
          );
        });
  }
}
