import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/features/filter/model/company.dart';
import 'package:bloodfinder/features/filter/model/department.dart';
import 'package:bloodfinder/global_model/model.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final donors = donorList;
  final groups = bloodGroups;

  var selectedBloodGroupIndex = 0.obs;

  final filterController = Get.put(FilterController());

  List<Donors> getFilteredDonorList() {
    var selectedFilterData = filterController.selectedFilterData;

    if (selectedBloodGroupIndex.value == 0 && selectedFilterData.isEmpty)
      return donors;

    if (selectedFilterData.isNotEmpty) {
      var list = donorList;

      if (filterController.filterData.bloodGroup.isNotEmpty) {
        list = list
            .where((element) =>
                element.bloodGroup == filterController.filterData.bloodGroup)
            .toList();
      }

      if (filterController.filterData.companies.isNotEmpty) {
        list = list.where((d) {
          var depts = <String>[];
          //
          // if (filterController.filterData.companies.contains(d.company)) {
          //   var cIndex = filterController.filterData.companies
          //       .indexWhere((cN) => d.name == cN.name);
          //   var c = filterController.filterData.companies[cIndex];
          //
          //   depts = c.depts.where((element) => element.isSelected);
          // }
          //
          // return filterController.filterData.companies. ;

          List<Company> c = filterController.filterData.companies
              .where((element) => element.name == d.company)
              .toList();

          if (c.isNotEmpty) {
            depts = c[0]
                .depts
                .where((element) => element.isSelected == true)
                .toList().map((e) => e.title).toList();
            return c[0].name == d.company && depts.contains(d.department);
          } else
            return false;
        }).toList();
      }

      if (filterController.filterData.gender.isNotEmpty) {
        list = list
            .where((element) =>
                element.gender == filterController.filterData.gender)
            .toList();
      }

      return list;
    }

    return donorList
        .where((element) =>
            element.bloodGroup == groups[selectedBloodGroupIndex.value])
        .toList();
  }
}
