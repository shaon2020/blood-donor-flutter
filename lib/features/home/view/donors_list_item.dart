import 'package:bloodfinder/features/donor_details/view/donor_details.dart';
import 'package:bloodfinder/global_model/model.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class DonorsListItem extends StatelessWidget {
  final Donors donors;

  const DonorsListItem(this.donors);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => DonorDetailsScreen()));
      },
      child: AbsorbPointer(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 20, top: 16),
          child: Column(
            children: [
              Row(
                children: [
                  ViewUtil.circularBoxContainer(
                    context,
                    40,
                    40,
                    30,
                    Center(child: ViewUtil.text(donors.bloodGroup, 14, FontWeight.w700, Colors.black,)),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ViewUtil.text(donors.name, 13,
                            FontWeight.w500, Colors.black),
                        SizedBox(
                          height: 6,
                        ),
                        ViewUtil.text(
                          donors.department,
                          11,
                          FontWeight.w400,
                          Color(0xff94989e),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  GestureDetector(
                    onTap: () {
                      _makePhoneCall('tel:+88${donors.phone}');
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                        right: 20,
                      ),
                      child: ViewUtil.circularBoxContainer(
                        context,
                        40,
                        40,
                        30,
                        Icon(
                          Icons.phone,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Divider(
                height: 1,
                color: Color(0xffe6e6e6),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
