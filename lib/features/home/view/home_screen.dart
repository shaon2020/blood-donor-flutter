import 'package:bloodfinder/features/filter/controller/filter_controller.dart';
import 'package:bloodfinder/features/filter/view/filter_main_page.dart';
import 'package:bloodfinder/features/home/controller/home_controller.dart';
import 'package:bloodfinder/features/home/view/donors_list_item.dart';
import 'package:bloodfinder/utils/view_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  static final routeName = "/home";
  final HomeController _homeController = Get.put(HomeController());
  final FilterController _filterController = Get.put(FilterController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: getAppbar(context),
          bottomNavigationBar: _filterController.selectedFilterData.isNotEmpty
              ? getBottomView(context)
              : Container(
                  height: 0,
                  width: 0,
                ),
          body: Column(
            children: [
              _filterController.selectedFilterData.isEmpty ?
              Container(
                width: double.infinity,
                height: 70,
                child: Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 40,
                      color: Color(0xffd1001c),
                    ),
                    getTopBloodGroupListView(),
                  ],
                ),
              ) :
              Container(
                height: 0,
                width: 0,
              ),
              showUserList(context),
            ],
          ),
        ));
  }

  getAppbar(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).accentColor,
      elevation: 0,
      title: Text(
        "BLOOD FINDER",
        style: TextStyle(
            color: Colors.white,
            fontFamily: 'Sofia-Pro-Semi-Bold.otf',
            fontSize: 16,
            fontWeight: FontWeight.w600),
      ),
      actions: [
        GestureDetector(
          onTap: () => Get.toNamed(FilterMainScreen.routeName),
          child: Container(
            margin: EdgeInsets.only(right: 20),
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                  Icons.filter_list,
                  color: Colors.white,
                )),
                Text(
                  "FILTERS",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Sofia-Pro-Semi-Bold.otf',
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  getTopBloodGroupListView() {
    return Container(
      margin: EdgeInsets.only(left: 10, top: 10),
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.horizontal(
            left: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
                color: const Color(0x1f000000),
                offset: Offset(0, 4),
                blurRadius: 15,
                spreadRadius: 0),
          ],
          color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _homeController.groups.length,
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                  _homeController.selectedBloodGroupIndex.value = index;
                },
                child: index == _homeController.selectedBloodGroupIndex.toInt()
                    ? Container(
                        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            )),
                        child: Center(
                          child: ViewUtil.text(_homeController.groups[index],
                              14, FontWeight.bold, Colors.white),
                        ),
                      )
                    : Container(
                        margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            )),
                        child: Center(
                          child: ViewUtil.text(_homeController.groups[index],
                              14, FontWeight.bold, Colors.black),
                        ),
                      ));
          },
        ),
      ),
    );
  }

  showUserList(BuildContext context) {
    return _homeController.getFilteredDonorList().isEmpty
        ? Expanded(
      child: Center(
          child: ViewUtil.text(
            "No Users",
            32,
            FontWeight.w700,
            Theme.of(context).accentColor,
          )),
    )
        : Expanded(
      child: ListView.builder(
        itemBuilder: (context, index) {
          return DonorsListItem(
              _homeController.getFilteredDonorList()[index]);
        },
        itemCount:
        _homeController.getFilteredDonorList().length,
      ),
    );
  }
  getBottomView(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _filterController.selectedFilterData.length,
              itemBuilder: (context, index) {
                return Center(
                  child: Container(
                    child: ViewUtil.text(
                        _filterController.selectedFilterData[index],
                        11,
                        FontWeight.w500,
                        Color(0xff686d73)),
                    margin: EdgeInsets.only(left: 6, top: 8, bottom: 8),
                    padding:
                        EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                    decoration: BoxDecoration(
                      color: Color(0xffededed),
                      borderRadius: BorderRadius.circular(13),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {
              _filterController.clear();
            },
            child: ViewUtil.text(
                "CLEAR", 12, FontWeight.w700, Theme.of(context).accentColor),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }
}
