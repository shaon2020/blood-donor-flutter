import 'dart:async';

import 'package:bloodfinder/features/home/view/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    });

    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
              child: Center(
                  child: SvgPicture.asset('assets/images/ic_splash_logo.svg'))),
          PositionedDirectional(
            bottom: 42,
            start: 16,
            end: 16,
            child: Image(
              height: 80,
              width: 200,
              image: AssetImage('assets/images/ic_splash_brands_logo.png'),
            ),
          ),
        ],
      ),
    );
  }
}
