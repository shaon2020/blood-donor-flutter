import 'package:bloodfinder/features/filter/model/department.dart';
import 'package:flutter/foundation.dart';

class Donors {
  final String id;
  final String name;
  final String department;
  final String company;
  final String phone;
  final String bloodGroup;
  final String gender;

  const Donors({
    this.id,
    this.name,
    this.department,
    this.company,
    this.phone,
    this.bloodGroup,
    this.gender,
  });
}

class BloodGroup {
  final String id;
  final String title;

  const BloodGroup({
    @required this.id,
    @required this.title,
  });
}

final bloodGroups = ["All", "A+", "B+", "A-", "B-", "AB+", "AB-", "O+", "O-"];
final bloodGroupsWithOutAll = [
  "A+",
  "B+",
  "A-",
  "B-",
  "AB+",
  "AB-",
  "O+",
  "O-"
];

final donorList = [
  Donors(
    id: "1",
    name: "Mohammad Masudur Rahman",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400159",
    bloodGroup: "A+",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Arafat Hossain",
    department: "Hr",
    company: "Bidyut Ltd.",
    phone: "01521400123",
    bloodGroup: "A+",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Arafat Hossain 2",
    department: "Accounts",
    company: "Bidyut Ltd.",
    phone: "01521400155",
    bloodGroup: "B+",
    gender: "Men",
  ),
  Donors(
    id: "3",
    name: "Mostaque Ahmed",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400222",
    bloodGroup: "B+",
    gender: "Men",
  ),
  Donors(
    id: "4",
    name: "Mohammad Masudur Rahman",
    department: "Finance",
    company: "SSL Wireless",
    phone: "01521400159",
    bloodGroup: "B-",
    gender: "Men",
  ),
  Donors(
    id: "5",
    name: "Fatema Noor Nazmoon",
    department: "Networking",
    company: "SSL Wireless",
    phone: "01521400159",
    bloodGroup: "AB+",
    gender: "Women",
  ),
  Donors(
    id: "6",
    name: "Fatema Noor Nazmoon 2",
    department: "Finance",
    company: "SSL Wireless",
    phone: "01521400154",
    bloodGroup: "AB-",
    gender: "Women",
  ),
  Donors(
    id: "1",
    name: "Mohammad Masudur Rahman",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400159",
    bloodGroup: "A+",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Arafat Hossain",
    department: "Hr",
    company: "Bidyut Ltd.",
    phone: "01521400123",
    bloodGroup: "A+",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Pinki",
    department: "Accounts",
    company: "Bidyut Ltd.",
    phone: "01521400155",
    bloodGroup: "AB-",
    gender: "Women",
  ),
  Donors(
    id: "3",
    name: "Mostaque Ahmed",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400222",
    bloodGroup: "AB-",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Arafat Hossain",
    department: "Hr",
    company: "Bidyut Ltd.",
    phone: "01521400123",
    bloodGroup: "A+",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Shamima",
    department: "Accounts",
    company: "Bidyut Ltd.",
    phone: "01521400155",
    bloodGroup: "B-",
    gender: "Women",
  ),
  Donors(
    id: "2",
    name: "Ramima",
    department: "Accounts",
    company: "Bidyut Ltd.",
    phone: "01521400155",
    bloodGroup: "B-",
    gender: "Women",
  ),
  Donors(
    id: "3",
    name: "Mostaque Ahmed",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400222",
    bloodGroup: "B-",
    gender: "Men",
  ),
  Donors(
    id: "2",
    name: "Shamima",
    department: "Accounts",
    company: "Bidyut Ltd.",
    phone: "01521400155",
    bloodGroup: "A-",
    gender: "Women",
  ),
  Donors(
    id: "3",
    name: "Mostaque Ahmed",
    department: "Engineering",
    company: "Concrode Garments Ltd.",
    phone: "01521400222",
    bloodGroup: "A-",
    gender: "Men",
  ),
  Donors(
    id: "4",
    name: "Mohammad Masudur Rahman 4",
    department: "Finance",
    company: "SSL Wireless",
    phone: "01521400159",
    bloodGroup: "O+",
    gender: "Men",
  ),
  Donors(
    id: "5",
    name: "Fatema Noor Nazmoon qwe",
    department: "Networking",
    company: "SSL Wireless",
    phone: "01521400159",
    bloodGroup: "O+",
    gender: "Women",
  ),
  Donors(
    id: "6",
    name: "Fatema Noor Nazmoon 2 2312",
    department: "Finance",
    company: "SSL Wireless",
    phone: "01521400154",
    bloodGroup: "O-",
    gender: "Women",
  ),
  Donors(
    id: "6",
    name: "Asrar",
    department: "Finance",
    company: "SSL Wireless",
    phone: "01521400154",
    bloodGroup: "O-",
    gender: "Men",
  ),
];
