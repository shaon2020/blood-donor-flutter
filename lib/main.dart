import 'package:bloodfinder/features/donor_details/view/donor_details.dart';
import 'package:bloodfinder/features/filter/view/filter_main_page.dart';
import 'package:bloodfinder/features/home/view/home_screen.dart';
import 'package:bloodfinder/features/splash/view/splash.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Blood Finder',
      theme: ThemeData(
        // adding 0xff before the hexa code
        primaryColor: Color(0xfff5f5f5) ,
        primaryColorDark: Color(0xffe1e1e1),
        // accentColor: Color(0xff999a9c),
        accentColor: Color(0xffd1001c),

        fontFamily: 'SofiaPro',
      ),
      initialRoute: '/',
      routes: {
        '/' : (ctx) => SplashScreen(),
        FilterMainScreen.routeName : (ctx) => FilterMainScreen(),
        DonorDetailsScreen.routeName : (ctx) => DonorDetailsScreen(),
        HomeScreen.routeName : (ctx) => HomeScreen(),
      },
    );
  }
}

