import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ViewUtil {
  static text(String str, double fontSize, FontWeight fontWeight, Color color) {
    return Text(str,
        style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontWeight: fontWeight,
          letterSpacing: 0,
        ));
  }

  static circularBoxContainer(BuildContext context, double width, double height,
      double radius, Widget child) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      child: child,
    );
  }
  static circularBoxContainerWithColor(BuildContext context, double width, double height,
      double radius, Widget child, Color color) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      child: child,
    );
  }
}
